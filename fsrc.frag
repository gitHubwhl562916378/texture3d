#version 330
uniform sampler3D sTexture;
in vec3 vPosition;
in vec4 vAmbient,vDiffuse,vSpecular;
out vec4 fragColor;

void main(void)
{
    //根据片元的位置折算出3D纹理坐标
    vec3 texCoor=vec3(((vPosition.x/0.2)+1.0)/2.0,vPosition.y/0.4,vPosition.z/0.4);
    vec4 noiseVec=texture(sTexture,texCoor);
//    vec4 noiseVec = vec4(0.1,0.5,0.0,1.0);
    fragColor = noiseVec*vAmbient+noiseVec*vSpecular+noiseVec*vDiffuse;
}
