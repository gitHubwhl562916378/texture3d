#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    render_.initsize();
    lightLocation_.setX(0.0);
    lightLocation_.setY(0.0);
    lightLocation_.setZ(5);
    cameraLocation_.setX(0.0);
    cameraLocation_.setY(1.0);
    cameraLocation_.setZ(1.5);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(30,float(w)/h,0.01f,100.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0f,0.0f,0.0f,1.0f);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(cameraLocation_,QVector3D(0.0,0.0,0.0),QVector3D(0.0,1.0,0.0));

    QMatrix4x4 mMatrix;
    mMatrix.rotate(-30,0,1,0);
    render_.render(f,pMatrix_,vMatrix,mMatrix,cameraLocation_,lightLocation_);
}
